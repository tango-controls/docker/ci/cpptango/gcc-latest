FROM gcc:14.1.0

ARG APP_UID=2000

ARG APP_GID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive
ENV CMAKE_PREFIX_PATH=/home/tango/lib/cmake
ENV PKG_CONFIG_PATH=/home/tango/lib/pkgconfig

RUN apt-get update -qq &&                      \
    apt-get install -y --no-install-recommends \
      abi-compliance-checker                   \
      abi-dumper                               \
      apt-utils                                \
      build-essential                          \
      bzip2                                    \
      cmake                                    \
      cppzmq-dev                               \
      curl                                     \
      gdb                                      \
      git                                      \
      libjpeg-dev                              \
      libssl-dev                               \
      libzmq3-dev                              \
      libzstd-dev                              \
      pkg-config                               \
      python3                                  \
      python3-dev                              \
      zlib1g-dev &&                            \
      rm -rf /var/lib/apt/lists/*

RUN groupadd -g "$APP_GID" tango &&                            \
    useradd -l -u "$APP_UID" -g "$APP_GID" -ms /bin/bash tango

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_GID" scripts/*.sh ./
COPY --chown="$APP_UID":"$APP_GID" scripts/patches/* ./patches/

RUN ./common_setup.sh      && \
    ./install_tango_idl.sh && \
    ./install_omniorb.sh &&   \
    ./install_catch.sh &&     \
    rm -rf dependencies
